import React from 'react';
import Homepage from './pages/homepage/homepage';

const App = () => {
    return (
        <div className="App">
            <h1>Hello World</h1>
            <Homepage/>
        </div>
    );
}

export default App;