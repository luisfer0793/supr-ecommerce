import React from 'react';

import NavigationBar from '../../components/NavigationBar/NavigationBar';

const Homepage = props => (
    <>
        <h2>Hi I'm the homepage</h2>
        <NavigationBar/>
    </>
);

export default Homepage;